import sys


data = sys.stdin.read().split('\n')


for str_num in data:
    if not str_num.isdigit():
        continue
    chislo = 1
    str_ch = str()
    chet = 0
    answer = 0
    while True:
        str_ch += str(chislo)
        chislo += 1
        answer = str_ch.find(str_num)
        if answer > -1:
            break
        if len(str_ch) > 100:
            chet += 50
            str_ch = str_ch[50:]
    sys.stdout.write(str(chet + answer + 1) + '\n')
