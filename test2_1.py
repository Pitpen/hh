import sys


def search_hollow(M, N, island):
    hollows = []
    for i in range(1, M - 1):
        for j in range(1, N - 1):
            now = island[i][j]
            if island[i][j + 1] < now:
                continue
            if island[i][j - 1] < now:
                continue
            if island[i + 1][j] < now or island[i - 1][j] < now:
                continue
            hollows.append([now, i, j])
    return hollows


def turn_around(y, x, hollows, island):
    now = island[y][x]
    prob_way = []
    if now >= island[y][x - 1]:
        if [y, x - 1] not in hollows:
            return False
        prob_way.append([y, x - 1])
    if now >= island[y][x + 1]:
        if [y, x + 1] not in hollows:
            return False
        prob_way.append([y, x + 1])
    if now >= island[y - 1][x]:
        if [y - 1, x] not in hollows:
            return False
        prob_way.append([y - 1, x])
    if now >= island[y + 1][x]:
        if [y + 1, x] not in hollows:
            return False
        prob_way.append([y + 1, x])
    return prob_way


def find_ways(hollows, island):
    no_ways = []
    yes_ways = []
    for y, x in hollows:
        if [y, x] in no_ways or [y, x] in yes_ways:
            continue
        flag = True
        unknown = []
        unknown.append([y, x])
        prob_way = turn_around(y, x, hollows, island)
        if prob_way is False:
            yes_ways.extend(unknown)
            continue
        if prob_way == []:
            no_ways.extend(unknown)
            continue
        unknown.extend(prob_way)
        while len(prob_way):
            y_1, x_1 = prob_way.pop(0)
            next_way = turn_around(y_1, x_1, hollows, island)
            if next_way is False:
                flag = False
                continue
            if next_way == []:
                continue
            for i in next_way:
                if i not in prob_way and i not in unknown:
                    prob_way.append(i)
                    unknown.append(i)
        if flag:
            no_ways.extend(unknown)
        else:
            yes_ways.extend(unknown)
    return yes_ways, no_ways

data = sys.stdin.read().split('\n')

cur_str = 1
for amount_island in range(int(data[0])):
    M, N = map(int, data[cur_str].split())
    island = [list(map(int, data[cur_str + 1 + i].split())) for i in range(M)]
    cur_str += M + 1
    answer = 0
    while True:
        hollows = search_hollow(M, N, island)
        hollows.sort(key=lambda x: x[0])
        hollows = [i[1:] for i in hollows]
        yes_way, no_way = find_ways(hollows, island)
        if not no_way:
            break
        for y, x in no_way:
            island[y][x] += 1
            answer += 1
    sys.stdout.write(str(answer) + '\n')
